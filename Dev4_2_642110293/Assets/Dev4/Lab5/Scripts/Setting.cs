using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

namespace Maneerat.GameDev4.Lab5.UIToolkit
{
    public class Setting : MonoBehaviour
    {
        private UIDocument _uiDocument;

        private VisualElement _startButton;
        
        // Start is called before the first frame update
        void Awake()
        {
            _uiDocument = FindObjectOfType<UIDocument>();
            _startButton = _uiDocument.rootVisualElement.Query<Button>("Setting");
        }

        private void OnEnable()
        {
            _startButton.RegisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }

        private void OnStartButtonMouseDownEvent(ClickEvent evt)
        {
            SceneManager.LoadSceneAsync("Setting");
        }

        private void OnDisable()
        {
            _startButton.UnregisterCallback<ClickEvent>(OnStartButtonMouseDownEvent);
        }
    }
}

